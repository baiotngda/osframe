package com.os.osframe.frame.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * 框架基础提示性页面
 * Created by wangdc on 2014-11-8.
 */

@Controller
@RequestMapping(value = "/feed")
public class JspController {
    /**
     * 直接访问jsp文件
     * @param request
     * @param feed
     * @return
     * @throws Exception
     */
    @RequestMapping("/{feed}")
    public String redirect(HttpServletRequest request,@PathVariable String feed)throws Exception {
        return "feed/"+feed;//路径获取为通用
    }

}
